use std::collections::HashMap;

use serde::{de::DeserializeOwned, Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Frames {
    pub data: Vec<u8>,
    pub width: usize,
    pub fps: f64,
    pub looping: bool,
}

impl Default for Frames {
    fn default() -> Self {
        Frames {
            data: vec![],
            width: 100,
            fps: 30.,
            looping: false,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PatternData {
    pub code: String,
    pub start: f64,
    pub end: f64,
    pub fps: f64,
    pub should_loop: bool,
}

impl Default for PatternData {
    fn default() -> Self {
        PatternData {
            code: "def bpm = 120\n\ncreate_array(100, fn(i) hsl_to_rgb([0.03, 1, 0.5]))".to_owned(),
            start: 0.,
            end: 5.,
            fps: 30.,
            should_loop: true,
        }
    }
}

#[derive(Serialize, Deserialize, Default)]
pub struct UserData {
    pub patterns: HashMap<String, (PatternData, Option<Vec<u8>>)>,
}

pub fn serialize<T: Serialize>(t: &T) -> eyre::Result<Vec<u8>> {
    let mut out = vec![];

    ciborium::into_writer(t, &mut out)?;

    Ok(out)
}

pub fn deserialize<T: DeserializeOwned + Default>(data: &[u8]) -> eyre::Result<T> {
    if data.is_empty() {
        return Ok(T::default());
    }

    Ok(ciborium::from_reader(data)?)
}

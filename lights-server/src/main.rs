use std::{
    collections::HashMap,
    path::Path,
    sync::Arc,
    time::{Duration, Instant},
};

use futures::TryStreamExt;
use log::{trace, warn};
use poem::{
    endpoint::StaticFilesEndpoint, handler, http::StatusCode, listener::TcpListener,
    middleware::AddData, post, web::Data, EndpointExt, IntoResponse, Response, Route, Server,
};
use rs_ws281x::{ChannelBuilder, ControllerBuilder, StripType};
use tokio::{
    fs,
    sync::{mpsc, RwLock},
};
use tokio_stream::wrappers::ReadDirStream;
use types::{deserialize, serialize, Frames, PatternData, UserData};

const LIGHTS_COUNT: i32 = 100;

#[handler]
async fn play_lights(
    lights_data: Vec<u8>,
    Data(tx): Data<&mpsc::UnboundedSender<Frames>>,
) -> impl IntoResponse {
    trace!("Playing pattern");

    let lights = match deserialize(&lights_data) {
        Ok(v) => v,
        Err(e) => {
            return Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body(format!("Failed to decode the body as frame data: {e}"))
        }
    };

    if tx.send(lights).is_err() {
        return Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body("The thread controlling the lights dropped the mpsc signaller");
    }

    Response::builder().status(StatusCode::OK).finish()
}

#[handler]
fn kill_lights(Data(tx): Data<&mpsc::UnboundedSender<Frames>>) -> impl IntoResponse {
    trace!("Killing lights");

    if tx
        .send(Frames {
            data: vec![0; 300],
            width: 100,
            fps: 30.,
            looping: false,
        })
        .is_err()
    {
        return Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body("The thread controlling the lights dropped the mpsc signaller");
    }

    Response::builder().status(StatusCode::OK).finish()
}

#[handler]
async fn get_user_data(
    poem::web::Path(user): poem::web::Path<String>,
    Data(users): Data<&Arc<Users>>,
) -> impl IntoResponse {
    trace!("Sending data for user {user}");

    let user_entry = match users.get(&user) {
        Some(v) => v,
        None => {
            return Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body("That username doesn't exist")
        }
    };

    let maybe_body = serialize(
        &user_entry
            .read()
            .await
            .patterns
            .iter()
            .collect::<HashMap<_, _>>(),
    );

    let body = match maybe_body {
        Ok(v) => v,
        Err(e) => {
            return Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(format!("Failed to serialize pattern data: {}", e))
        }
    };

    Response::builder().status(StatusCode::OK).body(body)
}

#[handler]
async fn set_pattern(
    poem::web::Path((user, pattern_name, what)): poem::web::Path<(String, String, String)>,
    lights_encoded: Vec<u8>,
    Data(users): Data<&Arc<Users>>,
    Data(dir): Data<&Arc<Path>>,
) -> impl IntoResponse {
    trace!("Setting {user}:{pattern_name}");

    let user_entry = match users.get(&user) {
        Some(v) => v,
        None => {
            return Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body("That username doesn't exist")
        }
    };

    let mut user_data = user_entry.write().await;

    let pattern_in_hashmap = user_data.patterns.get_mut(&pattern_name);

    if what == "pattern" {
        let pattern: PatternData = match deserialize(&lights_encoded) {
            Ok(v) => v,
            Err(e) => {
                return Response::builder()
                    .status(StatusCode::BAD_REQUEST)
                    .body(e.to_string())
            }
        };

        match pattern_in_hashmap {
            Some(v) => v.0 = pattern,
            None => {
                user_data.patterns.insert(pattern_name, (pattern, None));
            }
        }
    } else if what == "audio" {
        match pattern_in_hashmap {
            Some(v) => {
                v.1 = if lights_encoded.is_empty() {
                    None
                } else {
                    Some(lights_encoded)
                }
            }
            None => {}
        }
    }

    let serialized = match serialize(&*user_data) {
        Ok(v) => v,
        Err(e) => {
            return Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(e.to_string())
        }
    };

    if let Err(e) = fs::write(dir.join(user), serialized).await {
        return Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(e.to_string());
    };

    Response::builder().status(StatusCode::OK).body("")
}

#[handler]
async fn delete_pattern(
    poem::web::Path((user, pattern_name)): poem::web::Path<(String, String)>,
    Data(users): Data<&Arc<Users>>,
    Data(dir): Data<&Arc<Path>>,
) -> impl IntoResponse {
    trace!("Deleting {user}:{pattern_name}");

    let user_entry = match users.get(&user) {
        Some(v) => v,
        None => {
            return Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body("That username doesn't exist")
        }
    };

    let mut user_data = user_entry.write().await;

    user_data.patterns.remove(&pattern_name);

    let serialized = match serialize(&*user_data) {
        Ok(v) => v,
        Err(e) => {
            return Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(e.to_string())
        }
    };

    if let Err(e) = fs::write(dir.join(user), serialized).await {
        return Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(e.to_string());
    };

    Response::builder().status(StatusCode::OK).body("")
}

type Users = HashMap<String, RwLock<UserData>>;

#[tokio::main]
async fn main() -> eyre::Result<()> {
    pretty_env_logger::formatted_builder()
        .filter_level(log::LevelFilter::Trace)
        .init();

    let dir = Arc::from(
        dirs::data_dir()
            .ok_or_else(|| eyre::eyre!("Expected there to be a data dir"))?
            .join("lights-controller"),
    );

    let users: Users = ReadDirStream::new(fs::read_dir(Arc::clone(&dir)).await?)
        .map_err(eyre::Error::from)
        .and_then(|v| async move {
            Ok::<_, eyre::Error>((
                v.file_name()
                    .to_str()
                    .ok_or_else(|| eyre::eyre!("Expected the files to be utf8"))?
                    .to_string(),
                RwLock::new(deserialize(&fs::read(v.path()).await?)?),
            ))
        })
        .try_collect()
        .await?;

    let (tx, rx) = mpsc::unbounded_channel::<Frames>();

    tokio::task::spawn_blocking(|| lights_thread(rx));

    let app = Route::new()
        .nest(
            "/api",
            Route::new()
                .at("/play", post(play_lights))
                .at("/kill-lights", post(kill_lights))
                .at("/:user/user-data", post(get_user_data))
                .at("/:user/:name/set/:what", post(set_pattern))
                .at("/:user/:name/delete", post(delete_pattern)),
        )
        .nest(
            "/",
            StaticFilesEndpoint::new("../lights-frontend/dist").index_file("index.html"),
        )
        .with(AddData::new(Arc::new(users)))
        .with(AddData::new(dir))
        .with(AddData::new(tx));

    Server::new(TcpListener::bind("0.0.0.0:8080"))
        .run(app)
        .await?;

    Err(eyre::eyre!("Expected the server not to stop"))
}

fn lights_thread(mut rx: mpsc::UnboundedReceiver<Frames>) {
    let mut controller = match ControllerBuilder::new()
        .freq(800_000)
        .dma(10)
        .channel(
            0,
            ChannelBuilder::new()
                .pin(21)
                .count(LIGHTS_COUNT)
                .strip_type(StripType::Ws2811Bgr)
                .brightness(255)
                .invert(false)
                .build(),
        )
        .channel(
            1,
            ChannelBuilder::new()
                .pin(0)
                .count(0)
                .brightness(0)
                .invert(false)
                .build(),
        )
        .build()
    {
        Ok(v) => v,
        Err(rs_ws281x::WS2811Error::HwNotSupported) if cfg!(debug_assertions) => {
            warn!("Hardware not supported, allowing because debug_assertions is enabled");

            loop {
                std::thread::park();
            }
        }
        Err(e) => {
            panic!("{e}");
        }
    };

    let mut current_pattern: Option<Frames> = None;
    let mut frame_idx = 0;
    let mut start: Instant = Instant::now();

    loop {
        let pattern = match &mut current_pattern {
            Some(pattern) => {
                if let Ok(new_pattern) = rx.try_recv() {
                    if pattern.data.len() % (3 * pattern.width) != 0 {
                        continue;
                    }

                    *pattern = new_pattern;
                    frame_idx = 0;
                }

                pattern
            }
            None => {
                current_pattern = Some(rx.blocking_recv().expect("The transmitter not to drop"));
                frame_idx = 0;
                continue;
            }
        };

        std::thread::sleep(
            Duration::from_secs_f64(pattern.fps.recip())
                .checked_sub(start.elapsed())
                .unwrap_or(Duration::from_secs(0)),
        );
        start = Instant::now();

        let width = pattern.width;

        for (i, led) in controller.leds_mut(0).iter_mut().enumerate() {
            if i >= width {
                break;
            }
            let light = pattern
                .data
                .get((frame_idx * 3 * width + 3 * i)..(frame_idx * 3 * width + 3 * i + 3))
                .unwrap_or(&[0, 0, 0]);
            *led = [light[0], light[1], light[2], 0];
        }

        frame_idx += 1;

        if frame_idx >= pattern.data.len() * 3 * width {
            if pattern.looping {
                frame_idx = 0;
            } else {
                current_pattern = None;
            }
        }

        controller.render().expect("Rendering to work");
    }
}

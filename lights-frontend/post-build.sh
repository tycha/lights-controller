if [ ! -d "./target/ace" ] ; then
  git clone https://github.com/ajaxorg/ace-builds ./target/ace
else
  cd ./target/ace
  git pull
  cd ../..
fi

cp ./target/ace/src-noconflict/ace.js $TRUNK_STAGING_DIR/
cp ./target/ace/src-noconflict/theme-tomorrow_night.js $TRUNK_STAGING_DIR/

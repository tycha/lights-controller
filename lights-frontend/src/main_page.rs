use std::sync::Arc;

use futures::channel::mpsc::UnboundedSender;
use leptos::{
    component, create_rw_signal, event_target_value, view, CollectView, IntoView, RwSignal, Show,
    SignalGet, SignalGetUntracked, SignalSet, SignalUpdate, SignalWith,
};
use types::{Frames, PatternData};

use crate::files::Files;

#[component]
pub fn main_page(
    files: RwSignal<Files>,
    maybe_file_signal: RwSignal<
        Option<(Arc<str>, (RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>))>,
    >,
) -> impl IntoView {
    let (styler_class, style) = stylers::style_sheet_str!("./lights-frontend/src/main-page.css");

    let name = create_rw_signal(String::new());

    let is_valid =
        move || name.with(|n| !n.is_empty() && !files.with(|files| files.files.contains_key(&**n)));

    view! { class=styler_class,
        <style>{style}</style>
        <div class="container">
            <h1>Create Script</h1>
            <input
                prop:value=move || name.get()
                on:input=move |e| name.set(event_target_value(&e))
                placeholder="Name"
                on:keydown=move |e| {
                    if e.key() == "Enter" && is_valid() {
                        files
                            .update(|v| {
                                v
                                    .new_pattern(
                                        Arc::from(name.get_untracked()),
                                        (PatternData::default(), None),
                                    )
                            })
                    }
                }
            />
            <Show when=is_valid fallback=move || view! {}>
                <button on:click=move |_| {
                    files
                        .update(|v| {
                            v.new_pattern(Arc::from(name.get_untracked()), (PatternData::default(), None))
                        })
                }>Create</button>
            </Show>
            <h1>Open Script</h1>
            {move || {
                files
                    .with(|files_val| {
                        files_val
                            .files
                            .keys()
                            .cloned()
                            .map(|key| {
                                view! { <ScriptButton key files maybe_file_signal styler_class/> }
                            })
                            .collect_view()
                    })
            }}
            <div class="bottom-buttons">
                <KillLightsButton styler_class=styler_class files preview_tx={None}/>
            </div>
        </div>
    }
}

#[component]
fn script_button(
    styler_class: &'static str,
    key: Arc<str>,
    files: RwSignal<Files>,
    maybe_file_signal: RwSignal<
        Option<(Arc<str>, (RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>))>,
    >,
) -> impl IntoView {
    let key_for_on_click = Arc::clone(&key);
    let key_for_delete = Arc::clone(&key);

    view! { class=styler_class,
        <div class="script-button-container">
            <span
                class="script-button"
                on:click=move |_| {
                    let file = files
                        .with(|files| {
                            *files.files.get(&key_for_on_click).expect("the file to be there")
                        });
                    maybe_file_signal.set(Some((key_for_on_click.to_owned(), file)));
                }
            >
                {(*key).to_owned()}
            </span>
            <span
                class="trash-button"
                on:click=move |_| {
                    files.update(|files| files.remove(Arc::clone(&key_for_delete)));
                }
            >
                "🗑️"
            </span>
        </div>
    }
}

#[component]
pub fn kill_lights_button(
    styler_class: &'static str,
    files: RwSignal<Files>,
    preview_tx: Option<UnboundedSender<Frames>>,
) -> impl IntoView {
    view! { class=styler_class,
        <button on:click=move |_| files.with(|f| f.kill_lights(preview_tx.to_owned()))>Kill lights</button>
    }
}

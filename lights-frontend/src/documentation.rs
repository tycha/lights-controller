use indoc::indoc;
use leptos::*;

#[component]
pub fn documentation() -> impl IntoView {
    let (styler_class, style) = stylers::style_str!(
        pre {
            margin-top: 0;
            margin-left: 2rem;
        }

        h3 {
            margin-bottom: 8px;
        }
    );

    view! { class=styler_class,
        <style>{style}</style>
        <h1>Documentation</h1>
        <h2>Keybindings</h2>
        <pre>
            {
                indoc! {
                    "
        Ctrl-E to execute
        Ctrl-B to toggle documentation
        "
                }
            }
        </pre>
        <h2>Syntax</h2>
        <h3>Operators</h3>
        <pre>
            {
                indoc! {
                    "
        2 + 3 # 5
        2 - 3 # -1
        2 * 3 # 6
        2 / 3 # 0.6666...
        2 ^ 3 # 8
        2 % 3 # 2
        -(2 + 3) # -5
        -2 % 3 # 1, this is the actual modulus operator, not the cursed remainder operator you find in most other languages
        "
                }
            }
        </pre>
        <h3>Defining variables</h3>
        <pre>
            {
                indoc! {
                    "
        def v = 5 * global_time
        "
                }
            }
        </pre>
        <h3>Defining functions</h3>
        <pre>
            {
                indoc! {
                    "
        def f(x, y) = x * y

        f(2 3) # 6
        f(2, 3) # 6
        "
                }
            }
        </pre>
        <h3>Closures</h3>
        <pre>
            {
                indoc! {
                    "
        def call(f) = f(2, 3)

        call(fn(x y) x * y) # 6
        call(fn(x, y) x * y) # 6

        def mul(x, y) = x * y

        call(@mul) # Use \"@\" to turn a function into a closure
        "
                }
            }
        </pre>
        <h3>Scopes</h3>
        <pre>
            {
                indoc! {
                    "
        # Use parentheses to define a scope. Anything defined in the scope will be unavailable outside of it.
        def f(x) = (
            def v = x % 4

            v * 2
        )

        f(5) # 2
        v # Error
        "
                }
            }
        </pre>
        <h3>Lists</h3>
        <pre>
            {
                indoc! {
                    "
        [1, 2, 3] # Use brackets to define a list

        [1, 2, 3] * 2 % 4 # [2, 0, 2], most operations work on lists too
        "
                }
            }
        </pre>
        <h3>Sequences</h3>
        <pre>
            {
                indoc! {
                    "
        |1:1|2:1|3:1|4| switch
        # Sequences are defined by | expr : amt of beats | ... | expr | switch/add
        # If the sequence ends in \"switch\", every expression will be returned for however many beat are specified and then the last expression will be returned forever.
        # If the sequence ends in \"add\", the same thing happens except every previous expression is evaluated and added to the result.

        |local_beat:1|local_beat| switch
        # Sequences will reset local_beat and local_time. At both beats 0 & 1 the sequence will return zero.

        <1 : 2: 3> 2 beats; switch # Syntactic sugar for |1:8|2:6|3| switch
        # Functions like a midi track, each expr counts for (1 beat + the amount of whitespace after) * the amount of beats per space specified.
        # Spaces & colons both count as whitespace. Colons don\'t have special meaning.
        "
                }
            }
        </pre>
        <h3>Builtin functions and values</h3>
        <pre>
            {
                indoc! {
                    "
        pi # pi
        e # e
        create_array(len, fn(i) val) # Takes in a length and a closure that will be called with the index and the value it returns will be added to the list
        call(closure, arg1, arg2, ...) # Takes a closure and calls it with the given arguments, returning the value returned by the closure

        # These functions will all also accept lists of values and apply the operation to each value
        sin(v) # sin(v)
        cos(v) # cos(v)
        ln(v) # The natural log of v
        floor(v) # Round down v
        ceil(v) # Round up v
        round(v) # Round v to the nearest integer
        sign(v) # 1 if v is positive, -1 if v is negative, 0 if v is zero
        abs(v) # Take the absolute value of v
        log_base(v, b) # Take the log base b of v
        clamp(v, min, max) # Clamp v between a and b
        lerp(v, min, max) # Do a linear interpolation from a to b using value v between zero and one. Equivalent to min + v * (max - min)
        place_in(v, min, max) # Find the place of v in the range min to max as a value between zero and one.
                              # The inverse of lerp. Equivalent to (v - min) / (max - min).
                              # You can compose lerp and place_in to remap ranges: lerp(place_in(v, 1, 3), -1, 4)
        hsl_to_rgb(v) # Convert an hsl array to an rgb array. ([h, s, l] -> [r, g, b])
        "
                }
            }
        </pre>
    }
}

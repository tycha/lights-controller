#![feature(slice_flatten)]

use std::{rc::Rc, sync::Arc};

use editor::Editor;
use files::Files;
use leptos::*;
use types::PatternData;

use crate::main_page::MainPage;

mod documentation;
mod editor;
mod execution;
mod files;
mod main_page;

fn main() {
    _ = console_log::init_with_level(log::Level::Info);
    console_error_panic_hook::set_once();

    mount_to_body(app)
}

fn app() -> impl IntoView {
    let maybe_file_signal: RwSignal<
        Option<(Arc<str>, (RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>))>,
    > = create_rw_signal(None);
    let files_resource = create_local_resource(
        || (),
        move |_| async move { Rc::new(Files::get_from_server().await.map(create_rw_signal)) },
    );

    view! {
        {move || {
            files_resource
                .with(|files| match files.as_deref() {
                    Some(Ok(files)) => {
                        maybe_file_signal
                            .with(|maybe_file| match maybe_file {
                                Some((name, (file, audio))) => {
                                    view! {
                                        <Editor
                                            name=Arc::clone(name)
                                            file=*file
                                            audio=*audio
                                            go_back=move || maybe_file_signal.set(None)
                                            files=*files
                                        />
                                    }
                                        .into_view()
                                }
                                None => {
                                    view! { <MainPage maybe_file_signal files=*files/> }.into_view()
                                }
                            })
                            .into_view()
                    }
                    Some(Err(e)) => format!("Failed to load user data: {}", e).into_view(),
                    None => view! {}.into_view(),
                })
        }}
    }
}

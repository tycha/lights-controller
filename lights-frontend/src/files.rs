use std::{
    cell::Cell,
    collections::HashMap,
    hash::{DefaultHasher, Hasher},
    rc::Rc,
    sync::Arc,
};

use futures::channel::{mpsc::UnboundedSender, oneshot};
use js_sys::Uint8Array;
use leptos::{
    create_action, create_effect, create_rw_signal, spawn_local, window, Action, RwSignal,
    SignalGet, SignalGetUntracked, SignalWith,
};

use reqwest::Url;
use types::{deserialize, serialize, Frames, PatternData};
use wasm_bindgen::{closure::Closure, JsCast};
use web_sys::{AudioBuffer, AudioContext};

use crate::execution::execute_script;

pub struct Files {
    pub files: HashMap<Arc<str>, (RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>)>,
    user: Arc<str>,
    endpoint: Arc<Url>,
    play_action: Action<
        (
            Option<(RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>)>,
            UnboundedSender<Frames>,
        ),
        eyre::Result<()>,
    >,
    remove_action: Action<Arc<str>, eyre::Result<()>>,
    kill_lights_action: Action<Option<UnboundedSender<Frames>>, eyre::Result<()>>,
    server_sync_actions: RwSignal<Vec<Action<(), eyre::Result<()>>>>,
}

impl Files {
    pub fn play(&self, name: Arc<str>, sender: UnboundedSender<Frames>) {
        self.play_action
            .dispatch((self.files.get(&name).copied(), sender));
    }

    pub fn remove(&mut self, name: Arc<str>) {
        self.files.remove(&*name);

        self.remove_action.dispatch(name);
    }

    pub fn kill_lights(&self, sender: Option<UnboundedSender<Frames>>) {
        self.kill_lights_action.dispatch(sender);
    }

    pub fn new_pattern(
        &mut self,
        name: Arc<str>,
        (pattern, audio): (PatternData, Option<Vec<u8>>),
    ) {
        let pattern_signal = create_rw_signal(pattern);
        let audio_signal = create_rw_signal(audio);

        let endpoint = Arc::clone(&self.endpoint);
        let user = Arc::clone(&self.user);
        let name2 = Arc::clone(&name);
        let on_update = create_action(move |_| {
            let pattern = pattern_signal.get();

            let endpoint = Arc::clone(&endpoint);
            let user = Arc::clone(&user);
            let name = Arc::clone(&name2);

            let serialized = serialize(&pattern);

            async move {
                reqwest::Client::new()
                    .post(
                        endpoint
                            .join(&user)?
                            .join(&format!("{name}/"))?
                            .join("set/pattern")?,
                    )
                    .body(serialized?)
                    .send()
                    .await?;

                Ok::<_, eyre::Error>(())
            }
        });

        create_effect(move |_| {
            on_update.dispatch(());
        });

        let endpoint = Arc::clone(&self.endpoint);
        let user = Arc::clone(&self.user);
        let name2 = Arc::clone(&name);
        let on_audio_update = create_action(move |_| {
            let audio = audio_signal.get();

            let endpoint = Arc::clone(&endpoint);
            let user = Arc::clone(&user);
            let name = Arc::clone(&name2);

            async move {
                reqwest::Client::new()
                    .post(
                        endpoint
                            .join(&user)?
                            .join(&format!("{name}/"))?
                            .join("set/audio")?,
                    )
                    .body(match audio {
                        Some(v) => v,
                        None => vec![],
                    })
                    .send()
                    .await?;

                Ok::<_, eyre::Error>(())
            }
        });

        create_effect(move |_| {
            on_audio_update.dispatch(());
        });

        self.files.insert(name, (pattern_signal, audio_signal));
    }

    pub fn errors(&self) -> Vec<String> {
        let mut ret: Vec<String> = self
            .server_sync_actions
            .with(|v| v.iter().filter_map(|v| extract_error(*v)).collect());

        if let Some(e) = extract_error(self.play_action) {
            ret.push(e);
        }

        if let Some(e) = extract_error(self.remove_action) {
            ret.push(e);
        }

        if let Some(e) = extract_error(self.kill_lights_action) {
            ret.push(e);
        }

        ret
    }

    pub async fn get_from_server() -> eyre::Result<Files> {
        let location = window().location();
        let endpoint = Arc::new(
            Url::try_from(&*location.origin().expect("Getting origin to work"))?.join("api/")?,
        );

        let user = Arc::from(
            Url::try_from(&*location.href().expect("Getting href to work"))?
                .query_pairs()
                .next()
                .ok_or_else(|| eyre::eyre!("Must supply a query parameter for the name"))?
                .1
                + "/",
        );

        let patterns: HashMap<String, (PatternData, Option<Vec<u8>>)> = deserialize(
            &reqwest::Client::new()
                .post(endpoint.join(&user)?.join("user-data")?)
                .send()
                .await?
                .bytes()
                .await?,
        )?;

        let audio_cancel_cell = Rc::new(Cell::<Option<oneshot::Sender<()>>>::new(None));
        let audio_buffer_cell = Rc::new(Cell::<Option<(AudioBuffer, u64)>>::new(None));

        let endpoint2 = Arc::clone(&endpoint);
        let audio_cancel_cell_2 = Rc::clone(&audio_cancel_cell);
        let play_action = create_action(
            move |(maybe_signal, sender): &(
                Option<(RwSignal<PatternData>, RwSignal<Option<Vec<u8>>>)>,
                UnboundedSender<Frames>,
            )| {
                let sender = sender.to_owned();

                if let Some(tx) = audio_cancel_cell_2.replace(None) {
                    // I don't care if rx still exists
                    let _ = tx.send(());
                }

                let audio_context = AudioContext::new().unwrap();
                let audio_source = audio_context.create_buffer_source().unwrap();

                let maybe_executed = match maybe_signal {
                    Some((signal, audio_signal)) => {
                        let pattern = signal.get_untracked();

                        let maybe_audio_future =
                            audio_signal.with(|maybe_audio| match maybe_audio {
                                Some(ref audio) => {
                                    let audio_buffer = audio_buffer_cell.replace(None);
                                    audio_buffer_cell.set(audio_buffer.to_owned());

                                    let mut hasher = DefaultHasher::new();
                                    hasher.write(audio);
                                    let hash = hasher.finish();

                                    if audio_buffer.map_or(true, |v| v.1 != hash) {
                                        let array = Uint8Array::new_with_length(audio.len() as u32);

                                        array.copy_from(&audio);

                                        Some((
                                            wasm_bindgen_futures::JsFuture::from(
                                                audio_context
                                                    .decode_audio_data(&array.buffer())
                                                    .unwrap(),
                                            ),
                                            hash,
                                        ))
                                    } else {
                                        None
                                    }
                                }
                                None => None,
                            });

                        Some((
                            execute_script(
                                &pattern.code,
                                pattern.start,
                                pattern.end,
                                pattern.fps,
                                pattern.should_loop,
                            ),
                            pattern,
                            maybe_audio_future,
                        ))
                    }
                    None => None,
                };

                let endpoint = Arc::clone(&endpoint2);
                let audio_buffer_cell = Rc::clone(&audio_buffer_cell);

                let (tx1, rx1) = oneshot::channel();

                audio_cancel_cell_2.set(Some(tx1));

                async move {
                    match maybe_executed {
                        Some((executed, pattern, maybe_audio_future)) => {
                            if let Some(audio_future) = maybe_audio_future {
                                let audio = audio_future.0.await.unwrap().unchecked_into();
                                audio_buffer_cell.set(Some((audio, audio_future.1)));
                            }

                            let executed = executed?;

                            reqwest::Client::new()
                                .post(endpoint.join("play")?)
                                .body(serialize(&executed)?)
                                .send()
                                .await?;

                            let _ = sender.unbounded_send(executed);

                            if let Some(audio) = audio_buffer_cell.take() {
                                audio_buffer_cell.set(Some(audio.to_owned()));

                                let audio_buffer = audio.0;

                                let (tx2, rx2) = oneshot::channel();

                                let send_closure = Closure::<dyn FnOnce() -> ()>::once(move || {
                                    let _ = tx2.send(());
                                });

                                let timeout_id = window()
                                    .set_timeout_with_callback_and_timeout_and_arguments_0(
                                        send_closure.as_ref().unchecked_ref(),
                                        ((audio_buffer.duration().min(pattern.end) - pattern.start)
                                            * 1000.) as i32,
                                    )
                                    .unwrap();

                                audio_source.set_buffer(Some(&audio_buffer));
                                audio_source
                                    .connect_with_audio_node(&audio_context.destination())
                                    .unwrap();

                                audio_source.start_with_when(pattern.start).unwrap();

                                spawn_local(async move {
                                    let _ = futures::future::select(rx1, rx2).await;

                                    // Make sure the closure stays alive by taking ownership of it in here
                                    window().clear_timeout_with_handle(timeout_id);
                                    drop(send_closure);

                                    audio_source.stop().unwrap();
                                })
                            }

                            Ok(())
                        }
                        None => return Err(eyre::eyre!("The pattern name doesn't exist")),
                    }
                }
            },
        );

        let user2 = Arc::clone(&user);
        let endpoint2 = Arc::clone(&endpoint);
        let remove_action = create_action(move |name: &Arc<str>| {
            let name = Arc::clone(name);
            let user = Arc::clone(&user2);
            let endpoint = Arc::clone(&endpoint2);

            async move {
                reqwest::Client::new()
                    .post(endpoint.join(&user)?.join(&name)?.join("delete")?)
                    .send()
                    .await?;

                Ok(())
            }
        });

        let endpoint2 = Arc::clone(&endpoint);
        let kill_lights_action = create_action(move |sender: &Option<UnboundedSender<Frames>>| {
            if let Some(tx) = audio_cancel_cell.replace(None) {
                // I don't care if rx still exists
                let _ = tx.send(());
            }

            if let Some(sender) = sender {
                let _ = sender.unbounded_send(Frames {
                    data: vec![0; 300],
                    width: 100,
                    fps: 30.,
                    looping: false,
                });
            }

            let endpoint = Arc::clone(&endpoint2);

            async move {
                reqwest::Client::new()
                    .post(endpoint.join("kill_lights")?)
                    .send()
                    .await?;

                Ok(())
            }
        });

        let mut files = Files {
            files: HashMap::new(),
            user,
            endpoint,
            play_action,
            remove_action,
            kill_lights_action,
            server_sync_actions: create_rw_signal(vec![]),
        };

        for (name, data) in patterns {
            files.new_pattern(Arc::from(name), data);
        }

        Ok(files)
    }
}

fn extract_error<I, V>(action: Action<I, Result<V, eyre::Error>>) -> Option<String> {
    action
        .value()
        .with(|v| v.as_ref().map(|v| v.as_ref().err().map(|e| e.to_string())))
        .flatten()
}

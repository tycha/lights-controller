use light_programmer::{
    interpreting::Value,
    utils::{SafeFloat, SpanExt},
};
use types::Frames;

pub fn execute_script(
    script: &str,
    start: f64,
    end: f64,
    fps: f64,
    looping: bool,
) -> eyre::Result<Frames> {
    if start >= end {
        return Err(eyre::eyre!("The start time must be less than the end time"));
    }

    let parsed = light_programmer::parser::parse_script(script)?;

    let program = light_programmer::compiler::compile::<100>(parsed)?;

    let time_increment = fps.recip();

    let count = ((end - start) / time_increment).floor() as usize;

    let mut time = start;
    let mut data = vec![0; 3 * 100 * count];

    let beat_time_map = program.beat_time_map(end).map_err(|e| eyre::eyre!("{e}"))?;

    for data_idx in 0..count {
        match program
            .execute(
                &beat_time_map,
                light_programmer::utils::Span::new(script, 0, script.len())
                    .unwrap()
                    .with(SafeFloat::try_new_no_span(time).unwrap()),
            )
            .map_err(|e| eyre::eyre!("{e}"))?
        {
            Value::List(values) => {
                let values_parsed = values
                    .iter()
                    .map(|v| match v {
                        Value::List(colors) => <[u8; 3]>::try_from(
                            colors
                                .iter()
                                .map(|v| match v {
                                    Value::Number(n) => {
                                        Ok((n.inner() * 256.).clamp(0., 255.) as u8)
                                    }
                                    v => Err(eyre::eyre!("Expected a number, got {v:?}")),
                                })
                                .collect::<Result<Vec<u8>, _>>()?,
                        )
                        .map_err(|e| {
                            eyre::eyre!("Expected an array of 3 numbers, got {} numbers", e.len())
                        }),
                        v => Err(eyre::eyre!("Expected an array of 3 numbers, got {v:?}")),
                    })
                    .collect::<eyre::Result<Vec<[u8; 3]>>>()?;

                if values_parsed.len() != 100 {
                    return Err(eyre::eyre!(
                        "Expected an array of length 100, one for each light. Found {} elements",
                        values_parsed.len()
                    ));
                }

                data[3 * 100 * data_idx..3 * 100 * (data_idx + 1)]
                    .copy_from_slice(values_parsed.flatten());
            }
            v => {
                return Err(eyre::eyre!(
                    "Expected a array of arrays of three numbers, found {v:?}"
                ))
            }
        }

        time += time_increment;
    }

    Ok(Frames {
        data,
        width: 100,
        fps,
        looping,
    })
}

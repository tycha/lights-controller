use std::{rc::Rc, sync::Arc, time::Duration};

use futures::{
    channel::{
        mpsc::{self, UnboundedSender},
        oneshot,
    },
    StreamExt,
};
use js_sys::Uint8Array;
use leptos::{
    html::{Canvas, Input},
    *,
};

use pest::RuleType;
use serde::Serialize;
use types::{Frames, PatternData};
use wasm_bindgen::{
    prelude::{wasm_bindgen, Closure},
    JsCast, JsValue,
};
use wasm_bindgen_futures::JsFuture;
use web_sys::{CanvasRenderingContext2d, KeyboardEvent};

use crate::{documentation::Documentation, files::Files, main_page::KillLightsButton};

#[wasm_bindgen]
extern "C" {
    fn instrument_editor(initial_val: String, f: &Closure<dyn Fn(String)>) -> js_sys::Function;
}

#[derive(Serialize)]
struct Annotation {
    range: (usize, usize, usize, usize),
    text: String,
}

impl<T: RuleType> From<Box<pest::error::Error<T>>> for Annotation {
    fn from(e: Box<pest::error::Error<T>>) -> Self {
        use pest::error::LineColLocation::*;

        let range = match e.line_col {
            Pos(v) => (v.0, v.1, v.0, v.1),
            Span(s, e) => (s.0, s.1, e.0, e.1),
        };

        Annotation {
            range,
            text: e.variant.message().into_owned(),
        }
    }
}

#[wasm_bindgen]
pub fn get_errors(script: String) -> JsValue {
    let parsed = match light_programmer::parser::parse_script(&script) {
        Ok(v) => v,
        Err(e) => {
            return serde_wasm_bindgen::to_value(&Annotation::from(e))
                .expect("Serializing annotation to js to work");
        }
    };

    if let Err(e) = light_programmer::compiler::compile::<100>(parsed) {
        return serde_wasm_bindgen::to_value(&Annotation::from(e))
            .expect("Serializing annotation to js to work");
    }

    JsValue::NULL
}

#[derive(PartialEq, Eq, Clone, Copy)]
enum Mode {
    Editor,
    Documentation,
}

#[component]
pub fn editor<F: Fn() + 'static>(
    name: Arc<str>,
    files: RwSignal<Files>,
    file: RwSignal<PatternData>,
    audio: RwSignal<Option<Vec<u8>>>,
    go_back: F,
) -> impl IntoView {
    let (styler_class, style) = stylers::style_sheet_str!("./lights-frontend/src/editor.css");

    let start = move || file.with(|f| f.start);
    let end = move || file.with(|f| f.end);
    let fps = move || file.with(|f| f.fps);
    let should_loop = move || file.with(|f| f.should_loop);

    let mode = create_rw_signal(Mode::Editor);

    let code_setter = store_value(Rc::new(Closure::new(move |s| file.update(|f| f.code = s))));

    // Functionality not needed yet, but the code is there. The contents of the editor are initialized by `instrument_editor`, not by this.
    let mut _set_code: Box<dyn Fn(String)> =
        Box::new(|_| panic!("Tried to set the code before the editor was instrumented"));

    request_animation_frame(move || {
        let set_code_js = instrument_editor(
            file.with_untracked(|f| f.code.to_owned()),
            &code_setter.get_value(),
        );

        _set_code = Box::new(move |new_code| {
            set_code_js
                .call1(&JsValue::NULL, &JsValue::from_str(&new_code))
                .expect("Setting the code not to error");

            file.update(|f| f.code = new_code)
        })
    });

    let preview_canvas = create_node_ref::<Canvas>();

    let preview_tx = setup_preview_canvas(preview_canvas);
    let preview_tx2 = preview_tx.to_owned();
    let preview_tx3 = preview_tx.to_owned();

    let name2 = Arc::clone(&name);
    let _on_keypress = store_value(Rc::new(
        Closure::<dyn Fn(KeyboardEvent)>::new(move |e: KeyboardEvent| {
            if e.key() == "e" && e.ctrl_key() {
                files.with_untracked(|f| f.play(Arc::clone(&name2), preview_tx2.to_owned()));
                e.prevent_default();
            }

            if e.key() == "b" && e.ctrl_key() {
                mode.update(|mode| {
                    *mode = match mode {
                        Mode::Editor => Mode::Documentation,
                        Mode::Documentation => Mode::Editor,
                    }
                });
                e.prevent_default();
            }
        })
        .into_js_value()
        .dyn_into()
        .expect("Converting to a Function to work"),
    ));

    web_sys::window()
        .expect("there to be a window")
        .add_event_listener_with_callback("keydown", &_on_keypress.get_value())
        .expect("Adding an event listener to work");

    let file_input = create_node_ref::<Input>();

    let name_for_h1 = Arc::clone(&name);

    view! { class=styler_class,
        <style>{style}</style>
        <canvas class="preview" _ref={preview_canvas}></canvas>
        <div class="container">
            <div class="file-options">
                <h1>{move || name_for_h1.to_string()}</h1>
                <button on:click=move |_| {
                    let name = Arc::clone(&name);
                    files.with(|f| f.play(name, preview_tx3.to_owned()));
                }>Execute</button>
                <span class="separator"></span>
                <label>
                    Start
                    <input
                        type="number"
                        step="any"
                        prop:value=start
                        on:input=move |e| {
                            if let Ok(v) = event_target_value(&e).parse::<f64>() {
                                file.update(|f| f.start = v)
                            }
                        }
                    />
                </label>
                <label>
                    End
                    <input
                        type="number"
                        step="any"
                        prop:value=end
                        on:input=move |e| {
                            if let Ok(v) = event_target_value(&e).parse::<f64>() {
                                file.update(|f| f.end = v)
                            }
                        }
                    />
                </label>
                <label>
                    FPS
                    <input
                        type="number"
                        step="any"
                        prop:value=fps
                        on:input=move |e| {
                            if let Ok(v) = event_target_value(&e).parse::<f64>() {
                                file.update(|f| f.fps = v)
                            }
                        }
                    />
                </label>
                <label>
                    Should loop
                    <input
                        type="checkbox"
                        prop:checked=should_loop
                        on:input=move |e| file.update(|f| f.should_loop = event_target_checked(&e))
                    />
                </label>
                <span class="separator"></span>
                <span>
                    {move || {
                        if audio.with(|a| a.is_some()) {
                            "🔊 Has audio"
                        } else {
                            "🔇 No audio"
                        }
                    }}
                </span>
                <div class="audio-row">
                    <button on:click=move |_| {
                        file_input.get_untracked().unwrap().click();
                    }>Upload audio</button>
                    <Show when=move || audio.with(|a| a.is_some())>
                        <button on:click=move |_| {
                            audio.update(|a| *a = None);
                        }>Remove audio</button>
                    </Show>
                    <input
                        hidden
                        type="file"
                        accept="audio/*"
                        _ref={file_input}
                        on:change=move |_| {
                            let file_chosen = file_input
                                .get_untracked()
                                .unwrap()
                                .files()
                                .unwrap()
                                .get(0)
                                .unwrap();

                            let future = JsFuture::from(file_chosen.array_buffer());

                            spawn_local(async move {
                                let buf = future.await.unwrap();

                                let data = Uint8Array::new(&buf).to_vec();

                                audio.update(|a| *a = Some(data));
                            });
                        }
                    />
                </div>
                <pre class="error">
                    {move || files.with(|v| v.errors().get(0).map(|v| v.to_string()))}
                </pre>
            </div>
            <div id="editor" class="editor" hidden=move || mode.get() == Mode::Documentation></div>
            <div class="editor documentation" hidden=move || mode.get() == Mode::Editor>
                <Documentation/>
            </div>
        </div>
        <div class="bottom-buttons">
            <button on:click=move |_| {
                mode
                    .update(|mode| {
                        *mode = match mode {
                            Mode::Editor => Mode::Documentation,
                            Mode::Documentation => Mode::Editor,
                        };
                    })
            }>

                {move || match mode.get() {
                    Mode::Editor => "Documentation",
                    Mode::Documentation => "Editor",
                }}
            </button>
            <button on:click=move |_| go_back()>Back</button>
            <KillLightsButton styler_class files preview_tx={Some(preview_tx)}/>
        </div>
    }
}

fn setup_preview_canvas(preview_canvas: NodeRef<Canvas>) -> UnboundedSender<Frames> {
    let (preview_tx, mut rx) = mpsc::unbounded::<Frames>();

    preview_canvas.on_load(move |elt| {
        let ctx: CanvasRenderingContext2d =
            elt.get_context("2d").unwrap().unwrap().unchecked_into();

        ctx.set_fill_style(&"black".into());
        ctx.fill_rect(0., 0., elt.width() as f64, elt.height() as f64);

        spawn_local(async move {
            let mut current_pattern = None;
            let mut frame_idx = 0;
            let performance = window().performance().unwrap();
            let mut start = performance.now();

            loop {
                let pattern = match &mut current_pattern {
                    Some(pattern) => {
                        if let Ok(new_pattern) = rx.try_next() {
                            frame_idx = 0;
                            *pattern = new_pattern.expect("There to be a next frame if okay");
                        }

                        pattern
                    }
                    None => {
                        frame_idx = 0;
                        current_pattern =
                            Some(rx.next().await.expect("The transmitter not to drop"));

                        current_pattern.as_ref().unwrap()
                    }
                };

                let frame = match pattern.data.get(frame_idx..frame_idx + 300) {
                    Some(v) => v,
                    None => {
                        if pattern.looping {
                            frame_idx = 0;
                        } else {
                            current_pattern = None;
                        }

                        continue;
                    }
                };

                let block_height = elt.height() as f64;
                let block_width = elt.width() as f64 / 100.;

                for i in 0..100 {
                    let color = format!(
                        "rgb({},{},{})",
                        frame[i * 3 + 0],
                        frame[i * 3 + 1],
                        frame[i * 3 + 2]
                    );

                    ctx.set_fill_style(&color.into());

                    ctx.fill_rect(i as f64 * block_width, 0., block_width, block_height);
                }

                let (tx, rx) = oneshot::channel::<()>();

                set_timeout(
                    move || {
                        let _ = tx.send(());
                    },
                    Duration::from_millis(
                        (pattern.fps.recip() * 1000. - (performance.now() - start)) as u64,
                    ),
                );

                let _ = rx.await;

                start = performance.now();

                frame_idx += 300;
            }
        });
    });

    preview_tx
}

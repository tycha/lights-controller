use stylers::build;

fn main() {
    std::env::set_current_dir("..").unwrap();
    build(Some(String::from("./target/main.css")));
}
